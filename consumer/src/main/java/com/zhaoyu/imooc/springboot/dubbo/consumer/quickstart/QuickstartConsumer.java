package com.zhaoyu.imooc.springboot.dubbo.consumer.quickstart;

import com.alibaba.dubbo.config.annotation.Reference;
import com.zhaoyu.imooc.springboot.dubbo.ServiceAPI;
import org.springframework.stereotype.Component;

/**
 * @author linshimingyi
 * @package com.zhaoyu.imooc.springboot.dubbo.consumer.quickstart
 * @date 2019-02-19 13:00
 */

@Component
public class QuickstartConsumer {

    @Reference(interfaceClass = ServiceAPI.class)
    ServiceAPI serviceAPI;

    public void sendMessage(String message) {
        System.out.println(serviceAPI.sendMessage(message));
    }
}
