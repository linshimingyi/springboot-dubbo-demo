package com.zhaoyu.imooc.springboot.dubbo;

/**
 * @author linshimingyi
 * @package com.zhaoyu.imooc.springboot
 * @date 2019-02-18 22:29
 */
public interface ServiceAPI {

    String sendMessage(String message);
}
