package com.zhaoyu.imooc.springboot.dubbo.provider.quickstart;

import com.alibaba.dubbo.config.annotation.Service;
import com.zhaoyu.imooc.springboot.dubbo.ServiceAPI;
import org.springframework.stereotype.Component;

/**
 * @author linshimingyi
 * @package com.zhaoyu.imooc.springboot.dubbo.provider.quickstart
 * @date 2019-02-18 22:33
 */
@Component
@Service(interfaceClass = ServiceAPI.class)
public class QuickstartServiceImpl implements ServiceAPI {

    @Override
    public String sendMessage(String message) {
        return "quickstart-provider-message=" + message;
    }
}
